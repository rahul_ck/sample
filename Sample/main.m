//
//  main.m
//  Sample
//
//  Created by Nexegen Technologies on 16/10/13.
//  Copyright (c) 2013 Nexegen Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "nexegenAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([nexegenAppDelegate class]));
    }
}
