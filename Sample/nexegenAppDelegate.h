//
//  nexegenAppDelegate.h
//  Sample
//
//  Created by Nexegen Technologies on 16/10/13.
//  Copyright (c) 2013 Nexegen Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface nexegenAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
